// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Feeder;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.FeederSubsystem;

public class TimedFeederCommand extends CommandBase {
    private double startTime;
    private boolean isCompleted;
    private double time;
    private final FeederSubsystem feederSubsystem;
    /**
     * This runs the feeder forwards for a desired amount of time. This is what shoots the balls in
     * autonomous
     *
     * @param feederSubsystem The {@link FeederSubsystem} instance
     * @param time the desired time in seconds
     */
    public TimedFeederCommand(FeederSubsystem feederSubsystem, double time) {
        // Use addRequirements() here to declare subsystem dependencies.
        this.feederSubsystem = feederSubsystem;
        this.time = time;
        addRequirements(feederSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        isCompleted = false;
        startTime = System.currentTimeMillis();
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        feederSubsystem.turnFeederOn(true);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        feederSubsystem.setFeederOff();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        if (System.currentTimeMillis() - startTime >= time * 1000) {
            isCompleted = true;
        }
        return isCompleted;
    }
}
