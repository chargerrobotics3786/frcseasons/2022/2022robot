// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Feeder;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.FeederSubsystem;
import frc.robot.subsystems.FlywheelSubsystem;

public class FireWhenReadyCommand extends CommandBase {
    private boolean isCompleted = false;
    private FlywheelSubsystem flywheelSubsystem;
    private FeederSubsystem feederSubsystem;
    double firstToleranceTime = 0.0;
    /**
     * This runs the feeder motors when the flywheel's speed is within tolerance
     *
     * @param flywheelSubsystem The {@link FlywheelSubsystem} instance
     * @param feederSubsystem The {@link FeederSubsystem} instance
     */
    public FireWhenReadyCommand(
            FlywheelSubsystem flywheelSubsystem, FeederSubsystem feederSubsystem) {
        this.flywheelSubsystem = flywheelSubsystem;
        this.feederSubsystem = feederSubsystem;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(feederSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        isCompleted = false;
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        if (Math.abs(flywheelSubsystem.getRPMTarget() - flywheelSubsystem.getVelocity())
                <= Constants.Flywheel.READY_TO_FIRE_THRESHOLD) {
            feederSubsystem.setSpeed(1.0);
        } else {
            feederSubsystem.setFeederOff();
        }
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        feederSubsystem.setFeederOff();
        isCompleted = true;
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return isCompleted;
    }
}
