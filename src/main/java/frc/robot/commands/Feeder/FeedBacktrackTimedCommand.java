// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Feeder;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.FeederSubsystem;

public class FeedBacktrackTimedCommand extends CommandBase {
    private final FeederSubsystem feederSubsystem;
    private double startTime;
    private boolean isCompleted;
    /**
     * This runs the feeder backwards long enough for the ball to lose contact with the kicker
     * wheels
     *
     * @param feederSubsystem The {@link FeederSubsystem} instance
     */
    public FeedBacktrackTimedCommand(FeederSubsystem feederSubsystem) {
        // Use addRequirements() here to declare subsystem dependencies.
        this.feederSubsystem = feederSubsystem;
        addRequirements(feederSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        isCompleted = false;
        startTime = System.currentTimeMillis();
        feederSubsystem.setNeutralMode(NeutralMode.Brake);
        feederSubsystem.turnFeederOn(false);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {}

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        feederSubsystem.setFeederOff();
        feederSubsystem.setNeutralMode(NeutralMode.Coast);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        if (System.currentTimeMillis() - startTime
                >= Constants.Feeder.BACKTRACK_TIMED_DURATION_MS) {
            isCompleted = true;
        }
        return isCompleted;
    }
}
