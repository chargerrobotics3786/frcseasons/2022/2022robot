// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.kicker;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.KickerSubsystem;

public class KickerBacktrackCommand extends CommandBase {
    KickerSubsystem kickerSubsystem;
    /**
     * This command reverses the kicker wheels
     *
     * @param kickerSubsystem The {@link KickerSubsystem} instance
     */
    public KickerBacktrackCommand(KickerSubsystem kickerSubsystem) {
        // Use addRequirements() here to declare subsystem dependencies.
        this.kickerSubsystem = kickerSubsystem;
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        kickerSubsystem.setRunning(true, true);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {}

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        kickerSubsystem.setRunning(false, false);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}
