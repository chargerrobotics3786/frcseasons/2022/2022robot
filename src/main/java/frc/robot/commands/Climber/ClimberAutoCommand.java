// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Climber;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberAutoCommand extends CommandBase {
    private final ClimberSubsystem climberSubsystem;
    private double desiredPosition;
    /**
     * This command tells the robot to go to a desired position
     *
     * @param climberSubsystem The {@link ClimberSubsystem} instance
     * @param desiredPosition The desired climber position
     */
    public ClimberAutoCommand(ClimberSubsystem climberSubsystem, double desiredPosition) {
        // Use addRequirements() here to declare subsystem dependencies.
        this.climberSubsystem = climberSubsystem;
        this.desiredPosition = desiredPosition;
        addRequirements(climberSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {}

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        climberSubsystem.goToPosition(desiredPosition);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return true;
    }
}
