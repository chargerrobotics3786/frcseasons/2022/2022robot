package frc.robot.commands.Climber;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ClimberSubsystem;

/**
 * Climber command for manually moving motor in a direction (while holding control button)
 *
 * @author Tony P
 */
public class ClimberManualCommand extends CommandBase {
    // Contains the subsystem for use
    private final ClimberSubsystem climberSubsystem;

    // Parameter telling us the direction of the motor
    private boolean isUp;

    /**
     * @param climberSubsystem An instance of the climber subsystem
     * @param isUp Whether the climber should move up or down
     */
    public ClimberManualCommand(ClimberSubsystem climberSubsystem, boolean isUp) {
        this.climberSubsystem = climberSubsystem;
        this.isUp = isUp;
        addRequirements(climberSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {}

    /**
     * Called every time the scheduler runs while the command is scheduled.
     *
     * <p>Sets the speed to a constant percent output. Negative velocity if going down and positive
     * velocity if going up.
     */
    @Override
    public void execute() {
        climberSubsystem.setSpeed(
                isUp ? Constants.Climber.CONTROL_SPEED : -Constants.Climber.CONTROL_SPEED);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        climberSubsystem.setSpeed(0.0);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}
