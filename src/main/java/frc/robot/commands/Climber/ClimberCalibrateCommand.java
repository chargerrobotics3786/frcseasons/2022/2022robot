package frc.robot.commands.Climber;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ClimberSubsystem;

/**
 * Climber command for calibrating the mechanism
 *
 * @author Tony P
 */
public class ClimberCalibrateCommand extends CommandBase {
    private final ClimberSubsystem climberSubsystem;
    private boolean completed;

    /** @param climberSubsystem An instance of the climber subsystem */
    public ClimberCalibrateCommand(ClimberSubsystem climberSubsystem) {
        this.climberSubsystem = climberSubsystem;
        addRequirements(climberSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        completed = false;
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        climberSubsystem.setSpeed(-Constants.Climber.CALIBRATE_SPEED);
        completed = climberSubsystem.isLimitSwitchTriggered();
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        climberSubsystem.setSpeed(0.0);
        climberSubsystem.resetMotorEncoder();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return completed;
    }
}
