// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Flywheel;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.FlywheelSubsystem;

public class FlywheelOnCommand extends CommandBase {
    private final FlywheelSubsystem flywheelSubsystem;
    private double targetRPM;

    /**
     * This command turns on the flywheel to a desired RPM
     *
     * @param flywheelSubsystem The {@link FlywheelSubsystem} instance
     * @param targetRPM The desired RPM
     */
    public FlywheelOnCommand(FlywheelSubsystem flywheelSubsystem, double targetRPM) {
        this.flywheelSubsystem = flywheelSubsystem;
        this.targetRPM = targetRPM;
        addRequirements(flywheelSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        if (!Constants.ENABLE_TEST_BUTTON_MAPPINGS) {
            flywheelSubsystem.setRPMTarget(targetRPM);
        }
        flywheelSubsystem.setIsRunning(true);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return true;
    }
}
