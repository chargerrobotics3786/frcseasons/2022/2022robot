// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Flywheel;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.FlywheelSubsystem;

public class FlywheelSetpointCommand extends CommandBase {
    private final FlywheelSubsystem flywheelSubsystem;
    private int change;
    /**
     * This changes the flywheel's RPM setpoint by a desired amount. This is for testing purposes
     *
     * @param flywheelSubsystem The {@link FlywheelSubsystem} instance
     * @param change The amount of RPM to change the setpoint by
     */
    public FlywheelSetpointCommand(FlywheelSubsystem flywheelSubsystem, int change) {
        this.flywheelSubsystem = flywheelSubsystem;
        this.change = change;
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        flywheelSubsystem.setRPMTarget(flywheelSubsystem.getRPMTarget() + change);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return true;
    }
}
