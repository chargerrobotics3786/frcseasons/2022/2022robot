package frc.robot.commands.Intake;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.IntakeSubsystem;

public class IntakeAutoCommand extends CommandBase {
    private final IntakeSubsystem intakeSubsystem;
    private boolean isGoingup = false;

    /**
     * This moves the intake to its up or down position.
     *
     * @param intakeSubsystem THe {@link IntakeSubsystem} instance
     * @param isGoingUp flag for determining if the intake should move up or down
     */
    public IntakeAutoCommand(IntakeSubsystem intakeSubsystem, boolean isGoingUp) {
        this.intakeSubsystem = intakeSubsystem;
        this.isGoingup = isGoingUp;
        addRequirements(intakeSubsystem);
    }

    @Override
    public void initialize() {}

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        intakeSubsystem.setLifterPosition(
                isGoingup ? Constants.Intake.UP_POSITION : Constants.Intake.DOWN_POSITION);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return true;
    }
}
