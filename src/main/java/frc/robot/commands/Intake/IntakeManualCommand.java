package frc.robot.commands.Intake;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.IntakeSubsystem;

/**
 * Intake command for manually moving motor in a direction (while holding control button)
 *
 * @author Tony P
 */
public class IntakeManualCommand extends CommandBase {
    // Contains the subsystem for use
    private final IntakeSubsystem intakeSubsystem;

    // Parameter telling us the direction of the motor
    private boolean isUp;

    // Constructor that takes in subsystem and direction parameter
    public IntakeManualCommand(IntakeSubsystem intakeSubsystem, boolean isUp) {
        this.intakeSubsystem = intakeSubsystem;
        this.isUp = isUp;
        addRequirements(intakeSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {}

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        intakeSubsystem.setLifterSpeed(
                isUp ? Constants.Intake.UP_DOWN_SPEED : -Constants.Intake.UP_DOWN_SPEED);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        intakeSubsystem.setLifterSpeed(0.0);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}
