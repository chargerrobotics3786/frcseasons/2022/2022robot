// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.CommandGroups;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.FeederSubsystem;
import frc.robot.subsystems.FlywheelSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import frc.robot.subsystems.LimelightSubsystem;
import frc.robot.subsystems.ShooterHoodSubsystem;
// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html

public class TarmacShotCommandGroup extends SequentialCommandGroup {

    /**
     * This creates a {@link ReadyShooterCommandGroup} with setpoints for the high goal shot from
     * the tarmac's edge.
     *
     * @param feederSubsystem The {@link FeederSubsystem} instance
     * @param kickerSubsystem The {@link KickerSubsystem} instance
     * @param shooterHoodSubsystem The {@link ShooterHoodSubsystem} instance
     * @param flywheelSubsystem The {@link FlywheelSubsystem} instance
     */
    public TarmacShotCommandGroup(
            FeederSubsystem feederSubsystem,
            KickerSubsystem kickerSubsystem,
            ShooterHoodSubsystem shooterHoodSubsystem,
            FlywheelSubsystem flywheelSubsystem,
            LimelightSubsystem limelightSubsystem,
            DriveSubsystem driveSubsystem) {
        // Add your commands in the addCommands() call, e.g.
        // addCommands(new FooCommand(), new BarCommand());
        addCommands(
                new ParallelCommandGroup(
                        new ReadyShooterCommandGroup(
                                feederSubsystem,
                                kickerSubsystem,
                                flywheelSubsystem,
                                shooterHoodSubsystem,
                                Constants.Flywheel.TARMAC_RPM,
                                Constants.ShooterHood.TARMAC_LINE_POSITION)));
    }
}
