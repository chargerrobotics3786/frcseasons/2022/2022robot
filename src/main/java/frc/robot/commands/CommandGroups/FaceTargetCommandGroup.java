// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.CommandGroups;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.Limelight.LimelightOffCommand;
import frc.robot.commands.Limelight.LimelightOnCommand;
import frc.robot.commands.Limelight.TurnToTargetCommand;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.LimelightSubsystem;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class FaceTargetCommandGroup extends SequentialCommandGroup {
    /**
     * This command turns on the Limelight's LEDs, waits to aquire a target, turns to face the
     * target, stops, and turns off the Limelight LEDs
     *
     * @param limelightSubsystem The {@link LimelightSubsystem} instance
     * @param driveSubsystem The {@link DriveSubsystem} instance
     */
    public FaceTargetCommandGroup(
            LimelightSubsystem limelightSubsystem, DriveSubsystem driveSubsystem) {
        // Add your commands in the addCommands() call, e.g.
        // addCommands(new FooCommand(), new BarCommand());
        addCommands(
                new LimelightOnCommand(limelightSubsystem),
                new WaitCommand(Constants.Limelight.TARGET_AQUIRE_DELAY),
                new TurnToTargetCommand(limelightSubsystem, driveSubsystem),
                new LimelightOffCommand(limelightSubsystem));
    }
}
