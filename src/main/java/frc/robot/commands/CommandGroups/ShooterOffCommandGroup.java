// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.CommandGroups;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import frc.robot.commands.Feeder.FeederOffCommand;
import frc.robot.commands.Flywheel.FlywheelOffCommand;
import frc.robot.commands.Limelight.LimelightOffCommand;
import frc.robot.commands.kicker.KickerOffCommand;
import frc.robot.commands.shooterhood.HoodCalibrateCommand;
import frc.robot.subsystems.FeederSubsystem;
import frc.robot.subsystems.FlywheelSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import frc.robot.subsystems.LimelightSubsystem;
import frc.robot.subsystems.ShooterHoodSubsystem;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class ShooterOffCommandGroup extends ParallelCommandGroup {
    /**
     * This commandgroup stops the flywheel, calibrates the hood, turns off the Limelight LEDs,
     * turns off the kicker, and turns off the feeder
     *
     * @param flywheelSubsystem The{@link FlywheelSubsystem} instance
     * @param shooterHoodSubsystem The {@link ShooterHoodSubsystem} instance
     * @param kickerSubsystem The {@link KickerSubsystem} instance
     * @param feederSubsystem The {@link FeederSubsystem} instance
     * @param limelightSubsystem The {@link LimelightSubsystem} instance
     */
    public ShooterOffCommandGroup(
            FlywheelSubsystem flywheelSubsystem,
            ShooterHoodSubsystem shooterHoodSubsystem,
            KickerSubsystem kickerSubsystem,
            FeederSubsystem feederSubsystem,
            LimelightSubsystem limelightSubsystem) {
        // Add your commands in the addCommands() call, e.g.
        // addCommands(new FooCommand(), new BarCommand());
        addCommands(
                new LimelightOffCommand(limelightSubsystem),
                new FlywheelOffCommand(flywheelSubsystem),
                new HoodCalibrateCommand(shooterHoodSubsystem),
                new KickerOffCommand(kickerSubsystem),
                new FeederOffCommand(feederSubsystem));
    }
}
