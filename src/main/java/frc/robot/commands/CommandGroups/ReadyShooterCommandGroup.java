// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.CommandGroups;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.Feeder.FeedBacktrackTimedCommand;
import frc.robot.commands.Flywheel.FlywheelOnCommand;
import frc.robot.commands.kicker.KickerOnCommand;
import frc.robot.commands.shooterhood.HoodAutoCommand;
import frc.robot.subsystems.FeederSubsystem;
import frc.robot.subsystems.FlywheelSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import frc.robot.subsystems.ShooterHoodSubsystem;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class ReadyShooterCommandGroup extends SequentialCommandGroup {
    /**
     * This command group moves the ball down the feeder, spins up the shooter to a desired
     * setpoint, and moves the hood to the desired setpoint
     *
     * @param feederSubsystem The {@link FeederSubsystem} instance
     * @param kickerSubsystem The {@link KickerSubsystem} instance
     * @param flywheelSubsystem The {@link FlywheelSubsystem} instance
     * @param shooterHoodSubsystem The {@link ShooterHoodSubsystem} instance
     * @param flywheelRPM The desired RPM
     * @param hoodPosition The desired hood position
     */
    public ReadyShooterCommandGroup(
            FeederSubsystem feederSubsystem,
            KickerSubsystem kickerSubsystem,
            FlywheelSubsystem flywheelSubsystem,
            ShooterHoodSubsystem shooterHoodSubsystem,
            double flywheelRPM,
            double hoodPosition) {
        // Add your commands in the addCommands() call, e.g.
        // addCommands(new FooCommand(), new BarCommand());
        addCommands(
                new FeedBacktrackTimedCommand(feederSubsystem),
                new ParallelCommandGroup(
                        new KickerOnCommand(kickerSubsystem),
                        new FlywheelOnCommand(flywheelSubsystem, flywheelRPM),
                        new HoodAutoCommand(shooterHoodSubsystem, hoodPosition)));
    }
}
