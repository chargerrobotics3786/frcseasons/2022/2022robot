// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.CommandGroups.ReadyShooterCommandGroup;
import frc.robot.commands.Drivetrain.AutoDriveTimedCommand;
import frc.robot.commands.Feeder.TimedFeederCommand;
import frc.robot.commands.Flywheel.FlywheelOffCommand;
import frc.robot.commands.kicker.KickerOffCommand;
import frc.robot.commands.shooterhood.HoodCalibrateCommand;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.FeederSubsystem;
import frc.robot.subsystems.FlywheelSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import frc.robot.subsystems.ShooterHoodSubsystem;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class ShootOneBallMoveBackwards extends SequentialCommandGroup {
    /**
     * This command group makes the robot shoot 1 ball from the outer edge of the tarmac and back
     * out of the tarmac.
     *
     * @param driveSubsystem The {@link DriveSubsystem} instance
     * @param feederSubsystem The {@link FeederSubsystem} instance
     * @param kickerSubsystem The {@link KickerSubsystem} instance
     * @param flywheelSubsystem The {@link FlywheelSubsystem} instance
     * @param shooterHoodSubsystem The {@link ShooterHoodSubsystem} instance
     * @param intakeSubsystem The {@link IntakeSubsystem} instance
     */
    public ShootOneBallMoveBackwards(
            DriveSubsystem driveSubsystem,
            FeederSubsystem feederSubsystem,
            KickerSubsystem kickerSubsystem,
            FlywheelSubsystem flywheelSubsystem,
            ShooterHoodSubsystem shooterHoodSubsystem,
            IntakeSubsystem intakeSubsystem) {
        // Add your commands in the addCommands() call, e.g.
        // addCommands(new FooCommand(), new BarCommand());
        addCommands(
                new ReadyShooterCommandGroup(
                        feederSubsystem,
                        kickerSubsystem,
                        flywheelSubsystem,
                        shooterHoodSubsystem,
                        Constants.Flywheel.TARMAC_RPM,
                        Constants.ShooterHood.TARMAC_LINE_POSITION),
                new WaitCommand(3),
                new TimedFeederCommand(feederSubsystem, 1.5),
                new ParallelCommandGroup(
                        new FlywheelOffCommand(flywheelSubsystem),
                        new HoodCalibrateCommand(shooterHoodSubsystem),
                        new KickerOffCommand(kickerSubsystem)),
                new AutoDriveTimedCommand(driveSubsystem));
    }
}
