// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.CommandGroups.FaceTargetCommandGroup;
import frc.robot.commands.CommandGroups.ReadyShooterCommandGroup;
import frc.robot.commands.CommandGroups.ShooterOffCommandGroup;
import frc.robot.commands.Drivetrain.AutoDriveForwardCommand;
import frc.robot.commands.Drivetrain.AutoTurnL180Command;
import frc.robot.commands.Feeder.FeederOnCommand;
import frc.robot.commands.Feeder.TimedFeederCommand;
import frc.robot.commands.Intake.IntakeAutoCommand;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.FeederSubsystem;
import frc.robot.subsystems.FlywheelSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import frc.robot.subsystems.LimelightSubsystem;
import frc.robot.subsystems.ShooterHoodSubsystem;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class TwoBallCommand extends SequentialCommandGroup {
    /**
     * This command group makes the robot lower the intake, drive forward and collect a ball, turn
     * around, drive roughly back to the starting position, and shoot both balls.
     *
     * @param driveSubsystem The {@link DriveSubsystem} instance
     * @param feederSubsystem The {@link FeederSubsystem} instance
     * @param kickerSubsystem The {@link KickerSubsystem} instance
     * @param flywheelSubsystem The {@link FlywheelSubsystem} instance
     * @param shooterHoodSubsystem The {@link ShooterHoodSubsystem} instance
     * @param flywheelRPM The desired flywheel RPM
     * @param hoodPosition The desired hood position
     */
    public TwoBallCommand(
            DriveSubsystem driveSubsystem,
            FeederSubsystem feederSubsystem,
            KickerSubsystem kickerSubsystem,
            FlywheelSubsystem flywheelSubsystem,
            ShooterHoodSubsystem shooterHoodSubsystem,
            IntakeSubsystem intakeSubsystem,
            LimelightSubsystem limelightSubsystem) {
        // Add your commands in the addCommands() call, e.g.
        // addCommands(new FooCommand(), new BarCommand());
        addCommands(
                new IntakeAutoCommand(intakeSubsystem, false),
                new FeederOnCommand(feederSubsystem, true),
                new WaitCommand(2),
                new AutoDriveForwardCommand(driveSubsystem, 0.4, 0.75),
                new WaitCommand(1.5),
                new AutoTurnL180Command(driveSubsystem),
                new FaceTargetCommandGroup(limelightSubsystem, driveSubsystem),
                new AutoDriveForwardCommand(driveSubsystem, 0.4, 0.75),
                new ReadyShooterCommandGroup(
                        feederSubsystem,
                        kickerSubsystem,
                        flywheelSubsystem,
                        shooterHoodSubsystem,
                        Constants.Flywheel.TARMAC_RPM,
                        Constants.ShooterHood.TARMAC_LINE_POSITION),
                new WaitCommand(2),
                new TimedFeederCommand(feederSubsystem, 2.5),
                new ShooterOffCommandGroup(
                        flywheelSubsystem,
                        shooterHoodSubsystem,
                        kickerSubsystem,
                        feederSubsystem,
                        limelightSubsystem));
    }
}
