// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;

public class AutoDriveTimedCommand extends CommandBase {
    private final DriveSubsystem driveSubsystem;
    private double startTime;
    private boolean isCompleted;
    /**
     * This command drives the robot backwards so that it can move away from the tarmac
     *
     * @param driveSubsystem
     */
    public AutoDriveTimedCommand(DriveSubsystem driveSubsystem) {
        this.driveSubsystem = driveSubsystem;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(driveSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        isCompleted = false;
        startTime = System.currentTimeMillis();
        // move set speed here
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        driveSubsystem.setSpeeds(-0.2, -0.2);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        driveSubsystem.setSpeeds(0.0, 0.0);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        if (System.currentTimeMillis() - startTime >= 3000) {
            isCompleted = true;
        }
        return isCompleted;
    }
}
