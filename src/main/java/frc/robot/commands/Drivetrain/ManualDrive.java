// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Drivetrain;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.utils.XboxController;

/** Sends power to motors and kicks in the joystick batteries. */
public class ManualDrive extends CommandBase {
    @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
    private final DriveSubsystem m_DriveSubsystem;

    private XboxController primary;

    /**
     * Creates a new ManualDrive command.
     *
     * @param subsystem The {@link DriveSubsystem} instance
     * @param primary the primary XboxController
     */
    public ManualDrive(DriveSubsystem subsystem, XboxController primary) {
        m_DriveSubsystem = subsystem;
        this.primary = primary;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(subsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {}

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        m_DriveSubsystem.tankDrive(primary.getLeftStickY(), primary.getRightStickY());
        SmartDashboard.putNumber("R_Stick_Y", primary.getRightStickY());
        SmartDashboard.putNumber("L_Stick_Y", primary.getLeftStickY());
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}
