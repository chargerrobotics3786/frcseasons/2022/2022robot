// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;

public class AutoDriveForwardCommand extends CommandBase {
    private final DriveSubsystem driveSubsystem;
    private double startTime;
    private boolean isCompleted;
    private double speed;
    private double time;
    /**
     * This command drives the drivetrain motors for a desired time at a desired percent power.
     *
     * @param driveSubsystem The {@link Drivesubsystem} instance
     * @param speed The desired percent power of the motors
     * @param time The time in seconds that the motors should run for.
     */
    public AutoDriveForwardCommand(DriveSubsystem driveSubsystem, double speed, double time) {
        this.driveSubsystem = driveSubsystem;
        this.speed = speed;
        this.time = time;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(driveSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        isCompleted = false;
        startTime = System.currentTimeMillis();
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        driveSubsystem.setSpeeds(speed, speed);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        driveSubsystem.setSpeeds(0.0, 0.0);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        if (System.currentTimeMillis() - startTime >= time * 1000) {
            isCompleted = true;
        }
        return isCompleted;
    }
}
