// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;

public class AutoTurnL180Command extends CommandBase {
    double angle;
    DriveSubsystem driveSubsystem;
    boolean isCompleted;

    /**
     * This command turns the robot left 180 degrees.
     *
     * @param driveSubsystem The {@link DriveSubsystem} instance
     */
    public AutoTurnL180Command(DriveSubsystem driveSubsystem) {
        this.driveSubsystem = driveSubsystem;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(driveSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        isCompleted = false;
        driveSubsystem.setBrake(true);
        driveSubsystem.resetGyroHeading();
        driveSubsystem.setSpeeds(-0.2, 0.2);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        System.out.println(driveSubsystem.getRobotHeading());
        if ((driveSubsystem.getRobotHeading() > 10)) {
            isCompleted = true;
            driveSubsystem.setSpeeds(0, 0);
        } else {
            driveSubsystem.setSpeeds(-0.2, 0.2);
        }
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        driveSubsystem.tankDrive(0, 0);
        driveSubsystem.setBrake(false);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return isCompleted;
    }
}
