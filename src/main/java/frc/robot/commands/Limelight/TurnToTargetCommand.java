// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Limelight;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import frc.robot.Constants;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.LimelightSubsystem;
import frc.robot.utils.NetworkMapping;

public class TurnToTargetCommand extends PIDCommand {
    public final NetworkMapping<Double> kP =
            new NetworkMapping<Double>(
                    "Limelight_P",
                    Constants.Limelight.ALIGNMENT_P,
                    val -> {
                        setPIDP(val);
                    });
    public final NetworkMapping<Double> kI =
            new NetworkMapping<Double>(
                    "Limelight_I",
                    Constants.Limelight.ALIGNMENT_I,
                    val -> {
                        setPIDI(val);
                    });
    public final NetworkMapping<Double> kD =
            new NetworkMapping<Double>(
                    "Limelight_D",
                    Constants.Limelight.ALIGNMENT_D,
                    val -> {
                        setPIDD(val);
                    });
    private static LimelightSubsystem limelight;
    private static DriveSubsystem drive;
    private boolean isCompleted;
    private static PIDController pid;
    /**
     * This command uses PID control to turn the robot until the horizontal offset is within
     * tolerance and the rate at which the horizontal offset is changing is also in tolerance
     *
     * @param limelightSubsystem The {@link LimelightSubsystem} instance
     * @param driveSubsystem The {@link DriveSubsystem} instance
     */
    public TurnToTargetCommand(
            LimelightSubsystem limelightSubsystem, DriveSubsystem driveSubsystem) {
        // Use addRequirements() here to declare subsystem dependencies.
        super(
                // The controller that the command will use
                setPID(
                        new PIDController(
                                Constants.Limelight.ALIGNMENT_P,
                                Constants.Limelight.ALIGNMENT_I,
                                Constants.Limelight.ALIGNMENT_D)),
                // This should return the measurement
                () -> limelightSubsystem.getX(),
                // This should return the setpoint (can also be a constant)
                () -> 0,
                // This uses the output to move the robot
                output -> {
                    driveSubsystem.setSpeeds(-output, output);
                },
                limelightSubsystem);

        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(limelightSubsystem, driveSubsystem);
        // Configure additional PID options by calling `getController` here.
        limelight = limelightSubsystem;
        drive = driveSubsystem;
    }

    private static PIDController setPID(PIDController pid) {
        TurnToTargetCommand.pid = pid;
        return pid;
    }

    protected static double getPIDInput() {
        return limelight.getX();
    }

    private void setPIDP(double P) {
        pid.setP(P);
    }

    private void setPIDI(double I) {
        pid.setI(I);
    }

    private void setPIDD(double D) {
        pid.setD(D);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        super.initialize();
        isCompleted = false;
        limelight.setLEDStatus(true);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        if (limelight.getV() == 1) {
            super.execute();
            getController().getVelocityError();
        } else {
            isCompleted = true;
        }
        SmartDashboard.putNumber("Limelight Err Chng Rate", getController().getVelocityError());
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        super.end(interrupted);
        drive.setBrake(true);
        drive.setSpeeds(0, 0);
        drive.setBrake(false);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        if (Math.abs(limelight.getX()) < Constants.Limelight.TX_TOLERANCE
                && Math.abs(getController().getVelocityError())
                        <= Constants.Limelight.ERROR_CHANGE_RATE_TOLERANCE) {
            isCompleted = true;
        }
        return isCompleted;
    }
}
