package frc.robot.commands.shooterhood;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ShooterHoodSubsystem;

public class HoodAutoCommand extends CommandBase {
    private final ShooterHoodSubsystem shooterHoodSubsystem;
    private boolean isFinished = true;
    private double desiredPosition;

    /**
     * This command moves the shooter hood to a desired position
     *
     * @param shooterHoodSubsystem The {@link ShooterHoodSubsystem} instance
     * @param desiredPosition The desired hood's position in encoder ticks
     */
    public HoodAutoCommand(ShooterHoodSubsystem shooterHoodSubsystem, double desiredPosition) {
        this.shooterHoodSubsystem = shooterHoodSubsystem;
        this.desiredPosition = desiredPosition;
        addRequirements(shooterHoodSubsystem);
    }

    @Override
    public void initialize() {}

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        shooterHoodSubsystem.setPosition(desiredPosition);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return isFinished;
    }
}
