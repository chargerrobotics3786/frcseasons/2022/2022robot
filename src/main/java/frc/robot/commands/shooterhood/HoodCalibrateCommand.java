package frc.robot.commands.shooterhood;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ShooterHoodSubsystem;

public class HoodCalibrateCommand extends CommandBase {
    private final ShooterHoodSubsystem shooterHoodSubsystem;

    /**
     * This command moves the shooter hood down until its hall effect sensor is triggered. Then, the
     * hood will stop and reset its encoder.
     *
     * @param shooterHoodSubsystem The {@link ShooterHoodSubsystem} instance
     */
    public HoodCalibrateCommand(ShooterHoodSubsystem shooterHoodSubsystem) {
        this.shooterHoodSubsystem = shooterHoodSubsystem;
        addRequirements(shooterHoodSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        shooterHoodSubsystem.setNeutralMode(NeutralMode.Brake);
        shooterHoodSubsystem.setSpeed(Constants.ShooterHood.CALIBRATE_SPEED);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {}

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        shooterHoodSubsystem.setSpeed(0.0);
        shooterHoodSubsystem.resetShooterHoodEncoder();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return shooterHoodSubsystem.isLimitSwitchTriggered();
    }
}
