// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.shooterhood;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ShooterHoodSubsystem;

public class HoodManualCommand extends CommandBase {
    private final ShooterHoodSubsystem shooterHoodSubsystem;
    private boolean isGoingUp;

    /**
     * This command moves the hood up or down at a constant speed
     *
     * @param shooterHoodSubsystem The {@link ShooterHoodSubsystem} instance
     * @param isGoingUp boolean that determines if the hood should move up or down
     */
    public HoodManualCommand(ShooterHoodSubsystem shooterHoodSubsystem, boolean isGoingUp) {
        this.shooterHoodSubsystem = shooterHoodSubsystem;
        addRequirements(shooterHoodSubsystem);

        this.isGoingUp = isGoingUp;
        // Use addRequirements() here to declare subsystem dependencies.
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        if (isGoingUp) {
            shooterHoodSubsystem.setSpeed(Constants.ShooterHood.MANUAL_SPEED);
        } else {
            shooterHoodSubsystem.setSpeed(-Constants.ShooterHood.MANUAL_SPEED);
        }
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {}

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        shooterHoodSubsystem.setSpeed(0);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}
