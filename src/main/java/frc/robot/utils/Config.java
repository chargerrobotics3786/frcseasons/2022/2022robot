package frc.robot.utils;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import frc.robot.Constants;
import java.io.File;
import java.io.IOException;
import org.simpleyaml.configuration.file.YamlConfiguration;

public class Config {

    public static final NetworkTableInstance inst = NetworkTableInstance.getDefault();
    public static final NetworkTable table = inst.getTable("Config");

    private static final File configFile =
            new File(Constants.DATA_STORAGE_PATH, Constants.CONFIG_FILE_NAME);
    private static YamlConfiguration config;

    public static void setup() {
        inst.startClientTeam(Constants.TEAM);
        getConfig();
    }

    public static YamlConfiguration getConfig() {
        if (config == null) {
            reload();
        }
        return config;
    }

    public static void reload() {
        configFile.getParentFile().mkdirs();
        if (!configFile.isFile()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {

            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    public static void save() {
        if (config == null || configFile == null) {
            return;
        }
        try {
            getConfig().save(configFile);
        } catch (IOException e) {
            System.err.println("Could not save config to: " + configFile.getAbsolutePath());
            e.printStackTrace();
        }
    }
}
