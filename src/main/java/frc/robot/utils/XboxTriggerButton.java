package frc.robot.utils;

import edu.wpi.first.wpilibj2.command.button.Button;
import frc.robot.Constants;

/** Class for triggering commands from an Xbox controller's RT or LT trigger */
public class XboxTriggerButton extends Button {
    private edu.wpi.first.wpilibj.XboxController xboxController;
    private int axis;

    /**
     * Creates a button that triggers a command off an axis.
     *
     * @param controller The xbox controller to pass in
     * @param axis The trigger button's axis number
     */
    public XboxTriggerButton(edu.wpi.first.wpilibj.XboxController controller, int axis) {
        xboxController = controller;
        this.axis = axis;
    }

    /**
     * The trigger is considered "pressed" if it's held down past a threshold (found in {@link
     * Constants})
     */
    @Override
    public boolean get() {
        return xboxController.getRawAxis(axis) > Constants.XboxController.TRIGGER_BUTTON_THRESHOLD;
    }
}
