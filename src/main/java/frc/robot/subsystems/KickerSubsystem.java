// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class KickerSubsystem extends SubsystemBase implements Disableable {
    private static KickerSubsystem instance;
    private boolean isRunning;
    private boolean goBackwards;
    private WPI_TalonFX kickerMotor;

    /** Initializes the kicker motor by setting its neutral mode to brake and inverting it. */
    private KickerSubsystem() {
        kickerMotor = new WPI_TalonFX(Constants.Kicker.KICKER_MOTOR_ID);
        kickerMotor.setNeutralMode(NeutralMode.Brake);
        kickerMotor.setInverted(true);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
        SmartDashboard.putNumber("KickerMotor", kickerMotor.get());
    }

    /**
     * Spins the kicker motor in a desired direction and at a predermined speed defined in {@link
     * Constants}
     *
     * @param isRunning boolean for if the kicker motor should be on or not
     * @param goBackwards boolean for the kicker motor's direction. True is backwards, false is
     *     forwards
     */
    public void setRunning(boolean isRunning, boolean goBackwards) {
        this.isRunning = isRunning;
        this.goBackwards = goBackwards;
        if (this.isRunning) {
            if (goBackwards) {
                kickerMotor.set(ControlMode.PercentOutput, Constants.Kicker.BACKTRACK_SPEED);
            } else {
                kickerMotor.set(ControlMode.PercentOutput, Constants.Kicker.MANUAL_SPEED);
            }
        } else {
            kickerMotor.set(ControlMode.PercentOutput, 0);
        }
    }

    /** @return the {@link KickerSubsystem} singleton */
    public static KickerSubsystem getInstance() {
        if (instance == null) {
            instance = new KickerSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    /** Disables the {@link KickerSubsystem} by turning off the kicker motor. */
    public void disable() {
        setRunning(false, false);
    }
}
