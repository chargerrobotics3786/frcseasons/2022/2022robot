// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/** Creates a new FeederSubsystem. */
public class FeederSubsystem extends SubsystemBase {
    private static FeederSubsystem instance;
    private WPI_TalonFX collectorMotor;

    /** @return the {@link FeederSubsystem} singleton */
    public static FeederSubsystem getInstance() {
        if (instance == null) {
            instance = new FeederSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    /** Initializes the feeder motor, inverts it, and sets to coast mode */
    private FeederSubsystem() {
        collectorMotor = new WPI_TalonFX(Constants.Feeder.FEEDER_MOTOR_ID);
        collectorMotor.setInverted(true);
        collectorMotor.setNeutralMode(NeutralMode.Coast);
    }

    /**
     * Sets the {@link NeutralMode} in the feeder motor
     *
     * @param mode brake or coast
     */
    public void setNeutralMode(NeutralMode mode) {
        collectorMotor.setNeutralMode(mode);
    }

    /**
     * Sets the percent speed for the feeder motor
     *
     * @param speed the desired speed in percent
     */
    public void setSpeed(double speed) {
        collectorMotor.set(ControlMode.PercentOutput, speed);
    }

    /**
     * Sets the collector to a predetermined forwards or backwards speed defined in {@link
     * Constants.Feeder}
     *
     * @param isForward determines if the feeder motor should go forwards or backwards
     */
    public void turnFeederOn(boolean isForward) {
        if (isForward) {
            collectorMotor.set(ControlMode.PercentOutput, Constants.Feeder.FEED_SPEED);
        } else {
            collectorMotor.set(ControlMode.PercentOutput, Constants.Feeder.BACKTRACK_SPEED);
        }
    }

    /** Sends 0 power to the feeder motor */
    public void setFeederOff() {
        collectorMotor.set(ControlMode.PercentOutput, 0.0);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
    }
}
