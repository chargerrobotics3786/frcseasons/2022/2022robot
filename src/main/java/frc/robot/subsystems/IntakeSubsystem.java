package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.utils.NetworkMapping;

/**
 * Subsystem for controlling the intake mechanism.
 *
 * @author Tony P
 */
public class IntakeSubsystem extends SubsystemBase implements Disableable {
    public final NetworkMapping<Double> kP =
            new NetworkMapping<Double>(
                    Constants.Intake.KP_KEY,
                    Constants.Intake.kP,
                    val -> {
                        setPID_P(val);
                    });
    public final NetworkMapping<Double> kI =
            new NetworkMapping<Double>(
                    Constants.Intake.KI_KEY,
                    Constants.Intake.kI,
                    val -> {
                        setPID_I(val);
                    });
    public final NetworkMapping<Double> kD =
            new NetworkMapping<Double>(
                    Constants.Intake.KD_KEY,
                    Constants.Intake.kD,
                    val -> {
                        setPID_D(val);
                    });
    public final NetworkMapping<Double> kF =
            new NetworkMapping<Double>(
                    Constants.Intake.KF_KEY,
                    Constants.Intake.kF,
                    val -> {
                        setPID_F(val);
                    });

    // Stores the singleton subsystem instance
    private static IntakeSubsystem instance;

    // Hall effect sensor
    private DigitalInput intakeLimitSwitch = new DigitalInput(Constants.Intake.HALL_EFFECT_ID);

    // Falcon 500 motors
    private WPI_TalonFX lifterMotor;

    private boolean feederIsRunning = false;
    private boolean backtrackIsRunning = false;

    private double targetPosition;

    /** @return the singleton subsystem instance */
    public static IntakeSubsystem getInstance() {
        if (instance == null) {
            instance = new IntakeSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    private void setPID_P(double p) {
        lifterMotor.config_kP(Constants.Intake.PRIMARY_PID_LOOP, p);
    }

    private void setPID_I(double i) {
        lifterMotor.config_kI(Constants.Intake.PRIMARY_PID_LOOP, i);
    }

    private void setPID_D(double d) {
        lifterMotor.config_kD(Constants.Intake.PRIMARY_PID_LOOP, d);
    }

    private void setPID_F(double f) {
        lifterMotor.config_kF(Constants.Intake.PRIMARY_PID_LOOP, f);
    }

    /**
     * Constructor that initializes the lifter motor and encoder. The motor is inverted and set to
     * brake mode too.
     */
    public IntakeSubsystem() {
        lifterMotor = new WPI_TalonFX(Constants.Intake.UP_DOWN_MOTOR_ID);

        lifterMotor.configFactoryDefault();
        // lifterMotor.setSafetyEnabled(false);
        lifterMotor.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 0);
        lifterMotor.setNeutralMode(NeutralMode.Brake);
        lifterMotor.setInverted(true);
        resetEncoder();

        lifterMotor.config_kP(Constants.Intake.PRIMARY_PID_LOOP, Constants.Intake.kP);
        lifterMotor.config_kI(Constants.Intake.PRIMARY_PID_LOOP, Constants.Intake.kI);
        lifterMotor.config_kD(Constants.Intake.PRIMARY_PID_LOOP, Constants.Intake.kD);
        lifterMotor.config_kF(Constants.Intake.PRIMARY_PID_LOOP, Constants.Intake.kF);
    }

    /**
     * Sets the behavior of the motor when 0 percent power is sent
     *
     * @param neutralMode The desired {@link NeutralMode} of the lifter motor
     */
    public void setNeutralMode(NeutralMode neutralMode) {
        lifterMotor.setNeutralMode(neutralMode);
    }

    /**
     * Sends power to the lifter motor in percent
     *
     * @param speed The desired percent power
     */
    public void setSpeed(double speed) {
        lifterMotor.set(speed);
        SmartDashboard.putNumber("Collector Up/Down Speed", speed);
    }

    /** Returns the position of the lifter motor */
    public double getlifterMotorPosition() {
        return lifterMotor.getSelectedSensorPosition();
    }

    /** Returns a boolean based on if the IntakeSubsystem's Hall Effect Sensor is triggered. */
    public boolean isLimitSwitchTriggered() {
        return !intakeLimitSwitch.get();
    }

    /**
     * Sends power to the lifter motor in percent
     *
     * @param speed The desired percent power
     */
    public void setLifterSpeed(double speed) {
        lifterMotor.set(TalonFXControlMode.PercentOutput, speed);
    }

    /**
     * Lifter motor uses PID control to move to a desired position
     *
     * @param target The desired encoder position
     */
    public void setLifterPosition(double target) {
        lifterMotor.set(TalonFXControlMode.Position, target);
    }

    /** Sets the lifter motor's encoder position to 0 */
    public void resetEncoder() {
        lifterMotor.setSelectedSensorPosition(0);
    }

    /** Resets encoder if the limit switch is triggered for safety reasons. */
    @Override
    public void periodic() {
        super.periodic();
        SmartDashboard.putBoolean("Is Chomper switch trig?", isLimitSwitchTriggered());
        SmartDashboard.putNumber("Up down motor position", getlifterMotorPosition());

        if (isLimitSwitchTriggered()) {
            resetEncoder();
        }
    }

    @Override
    public void simulationPeriodic() {
        // This method will be called once per scheduler run during simulation
    }
    /** Disables the {@link IntakeSubsystem} by turning off the lifter motor. */
    @Override
    public void disable() {
        setLifterSpeed(0);
    }
}
