package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.utils.XboxController;

/**
 * Subsystem for controlling the robot's drive motors and interacting with the gyro
 *
 * @author Kyle B
 */
public class DriveSubsystem extends SubsystemBase implements Disableable {
    private static DriveSubsystem instance;

    private WPI_TalonFX leftRear;
    private WPI_TalonFX leftFront;
    private WPI_TalonFX rightRear;
    private WPI_TalonFX rightFront;

    private DifferentialDrive differentialDrive;

    private boolean brake;
    private boolean boost;
    private boolean slow;

    private boolean autonomousRunning;

    private AHRS ahrs;

    /**
     * @param driveController the driver's {@link XboxController}
     * @return the DriveSubsystem singleton
     */
    public static DriveSubsystem getInstance(XboxController driveController) {
        if (instance == null) instance = new DriveSubsystem(driveController);
        CommandScheduler.getInstance().registerSubsystem(instance);
        return instance;
    }

    /**
     * Creates a new DriveSubsystem.
     *
     * <p>Initializes the 6 drive motors, {@link DifferentialDrive} and gyro; inverts the right side
     * motors, sets current limits, ramp rates and resets encoders. For convenience, encoder values
     * are read from the front motors only since the rear motors follow the front motors.
     *
     * @param driveController the driver's {@link XboxController}
     */
    private DriveSubsystem(XboxController driveController) {

        leftRear = new WPI_TalonFX(Constants.Drivetrain.LEFTREAR_ID);
        leftFront = new WPI_TalonFX(Constants.Drivetrain.LEFTFRONT_ID);
        rightRear = new WPI_TalonFX(Constants.Drivetrain.RIGHTREAR_ID);
        rightFront = new WPI_TalonFX(Constants.Drivetrain.RIGHTFRONT_ID);
        rightFront.setInverted(true);
        rightRear.setInverted(true);
        leftRear.configGetSupplyCurrentLimit(
                new SupplyCurrentLimitConfiguration(
                        Constants.Drivetrain.CURRENT_LIMIT_ENABLED,
                        Constants.Drivetrain.CURRENT_LIMIT,
                        Constants.Drivetrain.CURRENT_LIMIT,
                        Constants.Drivetrain.CURRENT_LIMIT_TRIGGER_THRESHOLD_TIME));
        leftFront.configGetSupplyCurrentLimit(
                new SupplyCurrentLimitConfiguration(
                        Constants.Drivetrain.CURRENT_LIMIT_ENABLED,
                        Constants.Drivetrain.CURRENT_LIMIT,
                        Constants.Drivetrain.CURRENT_LIMIT,
                        Constants.Drivetrain.CURRENT_LIMIT_TRIGGER_THRESHOLD_TIME));
        rightRear.configGetSupplyCurrentLimit(
                new SupplyCurrentLimitConfiguration(
                        Constants.Drivetrain.CURRENT_LIMIT_ENABLED,
                        Constants.Drivetrain.CURRENT_LIMIT,
                        Constants.Drivetrain.CURRENT_LIMIT,
                        Constants.Drivetrain.CURRENT_LIMIT_TRIGGER_THRESHOLD_TIME));
        rightFront.configGetSupplyCurrentLimit(
                new SupplyCurrentLimitConfiguration(
                        Constants.Drivetrain.CURRENT_LIMIT_ENABLED,
                        Constants.Drivetrain.CURRENT_LIMIT,
                        Constants.Drivetrain.CURRENT_LIMIT,
                        Constants.Drivetrain.CURRENT_LIMIT_TRIGGER_THRESHOLD_TIME));

        leftRear.configOpenloopRamp(Constants.Drivetrain.OPEN_LOOP_RAMP);
        leftFront.configOpenloopRamp(Constants.Drivetrain.OPEN_LOOP_RAMP);
        rightRear.configOpenloopRamp(Constants.Drivetrain.OPEN_LOOP_RAMP);
        rightFront.configOpenloopRamp(Constants.Drivetrain.OPEN_LOOP_RAMP);

        leftRear.follow(leftFront); // makes it so the rear wheels do the same as the front wheels
        rightRear.follow(rightFront);

        resetEncoders();

        differentialDrive = new DifferentialDrive(leftFront, rightFront);
        differentialDrive.setDeadband(0.0);

        ahrs = new AHRS(SPI.Port.kMXP);
        ahrs.reset();
    }

    /**
     * Sets a longer ramp rate for the drive motors that would be ideal in autonomous mode.
     *
     * @param autonomousRunning
     */
    public void setAutonomousRunning(boolean autonomousRunning) {
        this.autonomousRunning = autonomousRunning;
        double rampRate = autonomousRunning ? 2.0 : 0.1;
        leftFront.configClosedloopRamp(rampRate);
        leftRear.configClosedloopRamp(rampRate);
        rightFront.configClosedloopRamp(rampRate);
        rightRear.configClosedloopRamp(rampRate);
    }

    /**
     * Sets a flag for determining if the robot should be in brake mode. Also sets the drive motors
     * to brake mode or coast mode depending on the flag.
     *
     * @param brake
     */
    public void setBrake(boolean brake) {
        this.brake = brake;
        if (this.brake) {
            leftFront.setNeutralMode(NeutralMode.Brake);
            rightFront.setNeutralMode(NeutralMode.Brake);
            leftRear.setNeutralMode(NeutralMode.Brake);
            rightRear.setNeutralMode(NeutralMode.Brake);
        } else {
            leftFront.setNeutralMode(NeutralMode.Coast);
            rightFront.setNeutralMode(NeutralMode.Coast);
            leftRear.setNeutralMode(NeutralMode.Coast);
            rightRear.setNeutralMode(NeutralMode.Coast);
        }
    }

    /**
     * Get the left drive motors' encoder position
     *
     * @return the left front motor's encoder position
     */
    public double getOdoLeft() {
        return leftFront.getSelectedSensorPosition();
    }

    /**
     * Get teh right drive motor's encoder position
     *
     * @return the right front motor's encoder position
     */
    public double getOdoRight() {
        return rightFront.getSelectedSensorPosition();
    }

    /** Sets the encoder positions to 0 for both sides of the drivetrain */
    public void resetEncoders() {
        leftFront.setSelectedSensorPosition(0);
        rightFront.setSelectedSensorPosition(0);
    }

    /** Resets the gyro heading to 0 */
    public void resetGyroHeading() {
        ahrs.reset();
    }

    /** @return the gyro's heading in degrees: (-180, 180) */
    public double getRobotHeading() {
        return ahrs.getYaw();
    }

    /**
     * Sets the speeds for the drive motors on each side
     *
     * @param left the left side's percent power
     * @param right the right side's percent power
     */
    public void setSpeeds(double left, double right) {
        leftFront.set(left);
        rightFront.set(right);
    }

    /**
     * Sets a flag for determining if the robot should be in boost mode
     *
     * @param boost
     */
    public void setBoost(boolean boost) {
        this.boost = boost;
    }

    /**
     * Sets a flag for determining if the robot should be in slow mode
     *
     * @param slow
     */
    public void setSlow(boolean slow) {
        this.slow = slow;
    }

    /**
     * Sends power to the motors in tank drive style
     *
     * <p>Brake mode takes priority and multiplies the joystick values by 0
     *
     * <p>Boost mode comes next and multiplies the joystick values by the maximum (boost mode)
     * percent power defined in {@link Constants.Drivetrain}
     *
     * <p>Slow mode's priority is after boost and multiplies the joystick values by the slow mode
     * percent power defined in {@link Constants.Drivetrain}
     *
     * @param leftPower the drive controller's left joystick Y value
     * @param rightPower the drive controller's right joystick Y value
     */
    public void tankDrive(double leftPower, double rightPower) {
        if (this.brake) {
            leftPower = 0.0;
            rightPower = 0.0;
        } else if (this.boost) {
            leftPower *= Constants.Drivetrain.BOOST_SPEED;
            rightPower *= Constants.Drivetrain.BOOST_SPEED;
        } else if (this.slow) {
            leftPower *= Constants.Drivetrain.SLOW_SPEED;
            rightPower *= Constants.Drivetrain.SLOW_SPEED;
        } else {
            leftPower *= Constants.Drivetrain.NORMAL_SPEED;
            rightPower *= Constants.Drivetrain.NORMAL_SPEED;
        }

        leftFront.set(ControlMode.PercentOutput, leftPower);
        rightFront.set(ControlMode.PercentOutput, rightPower);
        differentialDrive.feed();
    }

    /**
     * Sends power to the motors in arcade drive style
     *
     * @param throttle the left controller's left joystick Y value
     * @param turnRate the right controller's right joystick X value
     */
    public void arcadeDrive(double throttle, double turnRate) {
        differentialDrive.arcadeDrive(throttle, turnRate);
    }

    /** @return the percent power that was sent to the left side */
    public double getLeftSpeed() {
        return leftFront.getMotorOutputPercent();
    }

    /** @return the percent power that was sent to the right side */
    public double getRightSpeed() {
        return rightFront.getMotorOutputPercent();
    }

    @Override
    public void periodic() {
        super.periodic();
        if (Constants.DISPLAY_DIAGNOSTICS) {
            SmartDashboard.putBoolean("Slow", this.slow);
            SmartDashboard.putBoolean("Boost", this.boost);
            SmartDashboard.putBoolean("Brake", this.brake);
            SmartDashboard.putNumber("R_Speed", rightFront.getMotorOutputPercent());
            SmartDashboard.putNumber("L_Speed", leftFront.getMotorOutputPercent());
        }
        SmartDashboard.putNumber("heading", getRobotHeading());
        SmartDashboard.putBoolean("IMU_Connected", ahrs.isConnected());
        SmartDashboard.putBoolean("IMU_IsCalibrating", ahrs.isCalibrating());
    }

    /** Disables the {@link DriveSubsystem} by sending 0 power to the drive motors. */
    public void disable() {
        setSpeeds(0.0, 0.0);
    }
}
