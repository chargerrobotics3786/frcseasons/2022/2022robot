// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ShooterHoodSubsystem extends SubsystemBase implements Disableable {
    private WPI_TalonFX shooterHoodMotor;
    private static ShooterHoodSubsystem instance;
    private DigitalInput hallEffect =
            new DigitalInput(Constants.ShooterHood.HALL_EFFECT_SENSOR_PORT);
    private double targetPosition;

    public static ShooterHoodSubsystem getInstance() {
        if (instance == null) {
            instance = new ShooterHoodSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    /**
     * Creates a new ShooterHoodSubsystem
     *
     * <p>Initializes and configures the shooter hood motor by setting PID constants, setting an
     * error tolerance, inverting the motor, setting to brake mode, and resetting the encoder.
     */
    private ShooterHoodSubsystem() {
        shooterHoodMotor = new WPI_TalonFX(Constants.ShooterHood.SHOOTER_HOOD_MOTOR_ID);
        shooterHoodMotor.configSelectedFeedbackSensor(
                TalonFXFeedbackDevice.IntegratedSensor,
                Constants.ShooterHood.PRIMARY_PID_LOOP,
                Constants.ShooterHood.MOTOR_CONFIG_TIMEOUT_MS);
        shooterHoodMotor.setInverted(true);
        setPID_P(Constants.ShooterHood.KP);
        setPID_I(Constants.ShooterHood.KI);
        setPID_D(Constants.ShooterHood.KD);
        setPID_F(Constants.ShooterHood.KF);
        shooterHoodMotor.configAllowableClosedloopError(
                0, 500, Constants.ShooterHood.MOTOR_CONFIG_TIMEOUT_MS);
        shooterHoodMotor.setNeutralMode(NeutralMode.Brake);
        shooterHoodMotor.setInverted(true);
        resetShooterHoodEncoder();
    }

    private void setPID_P(double p) {
        shooterHoodMotor.config_kP(Constants.ShooterHood.PRIMARY_PID_LOOP, p);
    }

    private void setPID_I(double i) {
        shooterHoodMotor.config_kI(Constants.ShooterHood.PRIMARY_PID_LOOP, i);
    }

    private void setPID_D(double d) {
        shooterHoodMotor.config_kD(Constants.ShooterHood.PRIMARY_PID_LOOP, d);
    }

    private void setPID_F(double f) {
        shooterHoodMotor.config_kF(Constants.ShooterHood.PRIMARY_PID_LOOP, f);
    }

    /**
     * Sets the behavior of the motor when 0 percent power is sent
     *
     * @param neutralMode The desired {@link NeutralMode} of the shooter hood motor
     */
    public void setNeutralMode(NeutralMode neutralMode) {
        shooterHoodMotor.setNeutralMode(neutralMode);
    }

    /**
     * Sends power to the shooter hood motor in percent
     *
     * @param speed The desired percent power
     */
    public void setSpeed(double speed) {
        shooterHoodMotor.set(speed);
        SmartDashboard.putNumber("Hood Speed", speed);
    }

    /**
     * Shooter hood motor uses PID control to move to a desired position
     *
     * @param target The desired encoder position
     */
    public void setPosition(double targetPosition) {
        shooterHoodMotor.set(ControlMode.Position, targetPosition);
    }

    /** Sets the shooter hood motor's encoder position to 0 */
    public void setZero() {
        shooterHoodMotor.setSelectedSensorPosition(0);
    }

    /** Returns a boolean based on if the IntakeSubsystem's Hall Effect Sensor is triggered. */
    public boolean isLimitSwitchTriggered() {
        return !hallEffect.get();
    }

    /** @return the encoder position of the shooter hood motor */
    public double getEncoderPosition() {
        return shooterHoodMotor.getSelectedSensorPosition();
    }

    /** Sets the shooter hood motor's encoder position to 0 */
    public void resetShooterHoodEncoder() {
        shooterHoodMotor.setSelectedSensorPosition(0);
    }

    /**
     * Subtracts the shooter hood motor's current hood position from its desired position
     *
     * @return the position error in encoder counts.
     */
    public double getPositionError() {
        return targetPosition - shooterHoodMotor.getSelectedSensorPosition();
    }

    /** Resets encoder if the limit switch is triggered for safety reasons. */
    @Override
    public void periodic() {
        SmartDashboard.putBoolean("Hall Effect Hood", isLimitSwitchTriggered());
        SmartDashboard.putNumber("Hood Encoder Position", getEncoderPosition());
        if (isLimitSwitchTriggered()) {
            resetShooterHoodEncoder();
        }
    }

    /** Disables the {@link ShooterHoodSubsystem} by turning off the shooter hood motor. */
    public void disable() {
        setSpeed(0);
    }
}
