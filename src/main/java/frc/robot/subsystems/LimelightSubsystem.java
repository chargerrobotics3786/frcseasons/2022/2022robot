// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/**
 * Class for reading and interpreting Limelight data
 *
 * @author Nicholas L
 */
public class LimelightSubsystem extends SubsystemBase implements Disableable {
    private static LimelightSubsystem instance;

    private NetworkTable table;
    private NetworkTableEntry tv, tx, ty, ledMode, camMode;
    private double v, x, y;

    /** @return LimelightSubsystem singleton */
    public static LimelightSubsystem getInstance() {
        if (instance == null) {
            instance = new LimelightSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    /**
     * Creates a new LimelightSubsystem.
     *
     * <p>Gets the Limelight's network table
     */
    private LimelightSubsystem() {
        table = NetworkTableInstance.getDefault().getTable(Constants.Limelight.NETWORKTABLE_NAME);
        tv = table.getEntry(Constants.Limelight.TV_INDEX);
        tx = table.getEntry(Constants.Limelight.TX_INDEX);
        ty = table.getEntry(Constants.Limelight.TY_INDEX);
        ledMode = table.getEntry(Constants.Limelight.LEDMODE_INDEX);
        camMode = table.getEntry(Constants.Limelight.CAMMODE_INDEX);
        setLEDStatus(false);
    }

    /**
     * Turns the LED's on or off
     *
     * <p>If enabled, the Limelight switches to vision processing mode and uses the LED mode in the
     * vision pipeline
     *
     * <p>If disabled, the Limelight disables vision processing and increases exposure (Driver
     * Camera mode). Also forces the LED's to turn off
     *
     * @param isEnabled Should the LED's be on
     */
    public void setLEDStatus(boolean isEnabled) {
        ledMode.setNumber(
                isEnabled
                        ? Constants.Limelight.USE_PIPELINE_LED_MODE
                        : Constants.Limelight.FORCE_LEDS_OFF);
        camMode.setNumber(
                isEnabled
                        ? Constants.Limelight.VISION_PROCESSING_MODE
                        : Constants.Limelight.DRIVER_VISION_MODE);
    }

    /** @return the horizontal offset value from crosshair to target in degrees. */
    public double getX() {
        return x;
    }

    /** @return the distance along the floor from the Limelight to the target in meters */
    public double getDistanceMeters() {
        return (Constants.Limelight.UPPER_HUB_HEIGHT - Constants.Limelight.LIMELIGHT_HEIGHT)
                / Math.tan(Units.degreesToRadians(Constants.Limelight.LIMELIGHT_ANGLE + y));
    }

    /** @return the value of v. If the Limelight sees a target, 1 will be returned. Otherwise, 0 */
    public double getV() {
        return v;
    }

    /**
     * This method will be called once per scheduler run
     *
     * <p>Displays Limelight data on telemetry
     */
    @Override
    public void periodic() {
        v = tv.getDouble(-1); // -1 is our "invalid" value for v
        x = tx.getDouble(999); // 999 is our "invalid" value for x
        y = ty.getDouble(999); // 999 is also our "invalid" value for y

        SmartDashboard.putNumber("Limelight_V", v);
        SmartDashboard.putNumber("Limelight_X", x);
        SmartDashboard.putNumber("Limelight_Y", y);
        SmartDashboard.putNumber("Limelight Distance", getDistanceMeters());
    }

    /** Disables the {@link LimelightSubsystem} by turning off the LED's */
    public void disable() {
        setLEDStatus(false);
    }
}
