package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.utils.NetworkMapping;

/**
 * Subsystem for controlling the climber mechanism on the robot
 *
 * @author Tony P
 */
public class ClimberSubsystem extends SubsystemBase implements Disableable {
    public final NetworkMapping<Double> kP =
            new NetworkMapping<Double>(
                    Constants.Climber.KP_KEY,
                    Constants.Climber.KP,
                    val -> {
                        setPID_P(val);
                    });
    public final NetworkMapping<Double> kI =
            new NetworkMapping<Double>(
                    Constants.Climber.KI_KEY,
                    Constants.Climber.KI,
                    val -> {
                        setPID_I(val);
                    });
    public final NetworkMapping<Double> kD =
            new NetworkMapping<Double>(
                    Constants.Climber.KD_KEY,
                    Constants.Climber.KD,
                    val -> {
                        setPID_D(val);
                    });
    public final NetworkMapping<Double> kF =
            new NetworkMapping<Double>(
                    Constants.Climber.KF_KEY,
                    Constants.Climber.KF,
                    val -> {
                        setPID_F(val);
                    });
    private static ClimberSubsystem instance;
    private WPI_TalonFX climberMotor;
    private DigitalInput hallEffect = new DigitalInput(Constants.Climber.HALL_EFFECT_ID);

    /** @return ClimberSubsystem singleton */
    public static ClimberSubsystem getInstance() {
        if (instance == null) {
            instance = new ClimberSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    /**
     * Creates a new ClimberSubsystem.
     *
     * <p>Gets and configures the climber's motor
     */
    private ClimberSubsystem() {
        climberMotor = new WPI_TalonFX(Constants.Climber.MOTOR_ID);
        climberMotor.configFactoryDefault();
        climberMotor.setNeutralMode(NeutralMode.Brake);
        climberMotor.configSelectedFeedbackSensor(
                TalonFXFeedbackDevice.IntegratedSensor,
                Constants.Climber.PRIMARY_PID_LOOP,
                Constants.Climber.MOTOR_CONFIG_TIMEOUT_MS);
        setPID_P(Constants.Climber.KP);
        setPID_I(Constants.Climber.KI);
        setPID_D(Constants.Climber.KD);
        resetMotorEncoder();
    }

    /**
     * Updates the climber motor's P value
     *
     * @param p the "P" value
     */
    public void setPID_P(double p) {
        climberMotor.config_kP(Constants.Climber.PRIMARY_PID_LOOP, p);
    }

    /**
     * Updates the climber motor's I value
     *
     * @param i the "I" value
     */
    public void setPID_I(double i) {
        climberMotor.config_kI(Constants.Climber.PRIMARY_PID_LOOP, i);
    }

    /**
     * Updates the climber motor's D value
     *
     * @param d the "D" value
     */
    public void setPID_D(double d) {
        climberMotor.config_kD(Constants.Climber.PRIMARY_PID_LOOP, d);
    }

    /**
     * Updates the climber motor's F value
     *
     * @param f the "F" value
     */
    public void setPID_F(double f) {
        climberMotor.config_kF(Constants.Climber.PRIMARY_PID_LOOP, f);
    }

    /** @return Whether the hall effect sensor is triggered or not */
    public boolean isLimitSwitchTriggered() {
        return !hallEffect.get();
    }

    /**
     * Sets the encoder position the climber motor should be at
     *
     * @param desiredPosition the desired encoder value
     */
    public void goToPosition(double desiredPosition) {
        climberMotor.set(ControlMode.Position, desiredPosition);
    }

    /** @return The position of the climber's motor */
    public double getEncoderPosition() {
        return climberMotor.getSelectedSensorPosition();
    }

    /**
     * Zeroes the motor encoder's position
     *
     * <p>This method is invoked when the subsystem is calibrated.
     */
    public void resetMotorEncoder() {
        climberMotor.setSelectedSensorPosition(0);
    }

    /**
     * In order to move the motor up or down, this method is invoked
     *
     * @param speed The percent power given to the climber's motor between -1.0 and 1.0
     */
    public void setSpeed(double speed) {
        climberMotor.set(TalonFXControlMode.PercentOutput, speed);
    }

    /**
     * This method will be called once per scheduler run
     *
     * <p>Displays Climber data on telemetry
     */
    @Override
    public void periodic() {
        SmartDashboard.putBoolean("Hall Effect Climber", isLimitSwitchTriggered());
        SmartDashboard.putNumber("Climber Encoder Position", getEncoderPosition());
    }

    /** Disables the {@link ClimberSubsystem} by turning off the motor */
    @Override
    public void disable() {
        climberMotor.set(TalonFXControlMode.PercentOutput, 0.0);
    }
}
