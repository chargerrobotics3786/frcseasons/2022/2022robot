// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.utils.NetworkMapping;

/**
 * Class for controlling the flywheel.
 *
 * @author Nicholas L
 */
public class FlywheelSubsystem extends SubsystemBase implements Disableable {
    public final NetworkMapping<Double> kP =
            new NetworkMapping<Double>(
                    Constants.Flywheel.KP_KEY,
                    Constants.Flywheel.KP,
                    val -> {
                        setPID_P(val);
                    });
    public final NetworkMapping<Double> kI =
            new NetworkMapping<Double>(
                    Constants.Flywheel.KI_KEY,
                    Constants.Flywheel.KI,
                    val -> {
                        setPID_I(val);
                    });
    public final NetworkMapping<Double> kD =
            new NetworkMapping<Double>(
                    Constants.Flywheel.KD_KEY,
                    Constants.Flywheel.KD,
                    val -> {
                        setPID_D(val);
                    });
    public final NetworkMapping<Double> kF =
            new NetworkMapping<Double>(
                    Constants.Flywheel.KF_KEY,
                    Constants.Flywheel.KF,
                    val -> {
                        setPID_F(val);
                    });
    public final NetworkMapping<Double> kSetPoint =
            new NetworkMapping<Double>(
                    Constants.Flywheel.SETPOINT_KEY,
                    0.0,
                    val -> {
                        rpmTarget = val;
                    });

    private static FlywheelSubsystem instance;
    private WPI_TalonFX flywheel;
    private double rpmTarget;
    private boolean isRunning;

    /** @return FlywheelSubsystem singleton. */
    public static FlywheelSubsystem getInstance() {
        if (instance == null) {
            instance = new FlywheelSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    /**
     * Creates a new FlywheelSubsystem.
     *
     * <p>Initializes a single Talon FX and configures it to use the built in encoder, have a ramp
     * rate, and limit its supply current.
     */
    private FlywheelSubsystem() {
        flywheel = new WPI_TalonFX(Constants.Flywheel.FLYWHEEL_ID);
        flywheel.configSelectedFeedbackSensor(
                TalonFXFeedbackDevice.IntegratedSensor,
                Constants.Flywheel.PRIMARY_PID_LOOP,
                Constants.Flywheel.MOTOR_CONFIG_TIMEOUT_MS);
        setPID_P(Constants.Flywheel.KP);
        setPID_I(Constants.Flywheel.KI);
        setPID_D(Constants.Flywheel.KD);
        setPID_F(Constants.Flywheel.KF);
        flywheel.configClosedloopRamp(Constants.Flywheel.RAMP_RATE_SECONDS);
        flywheel.configSupplyCurrentLimit(
                new SupplyCurrentLimitConfiguration(
                        Constants.Flywheel.CURRENT_LIMIT_ENABLED,
                        Constants.Flywheel.CURRENT_LIMIT,
                        Constants.Flywheel.CURRENT_LIMIT,
                        Constants.Flywheel.CURRENT_LIMIT_TRIGGER_THRESHOLD_TIME));
        isRunning = false;
    }

    private void setPID_P(double p) {
        flywheel.config_kP(Constants.Flywheel.PRIMARY_PID_LOOP, p);
    }

    private void setPID_I(double i) {
        flywheel.config_kI(Constants.Flywheel.PRIMARY_PID_LOOP, i);
    }

    private void setPID_D(double d) {
        flywheel.config_kD(Constants.Flywheel.PRIMARY_PID_LOOP, d);
    }

    private void setPID_F(double f) {
        flywheel.config_kF(Constants.Flywheel.PRIMARY_PID_LOOP, f);
    }

    /**
     * Sets a flag for determining if the flywheel should be turned on or off.
     *
     * @param isRunning
     */
    public void setIsRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    /**
     * Drives the flywheel motor to the desired velocity.
     *
     * @param rpm Desired RPM
     */
    public void setVelocity(double rpm) {
        flywheel.set(
                ControlMode.Velocity,
                rpm * Constants.Flywheel.RPM_CONVERSION * Constants.Flywheel.GEAR_REDUCTION);
    }

    /**
     * Stores the desired RPM for later use until the flywheel motor turns on.
     *
     * @param target RPM to store
     */
    public void setRPMTarget(double target) {
        rpmTarget = target;
    }

    /** Sets the flywheel motor to 0 percent power */
    public void turnOffFlywheel() {
        flywheel.set(ControlMode.PercentOutput, 0.0);
    }

    /**
     * Gets the flywheel's velocity.
     *
     * @return RPM measured from the built in encoder.
     */
    public double getVelocity() {
        return flywheel.getSelectedSensorVelocity()
                / Constants.Flywheel.RPM_CONVERSION
                / Constants.Flywheel.GEAR_REDUCTION;
    }

    /**
     * Gets the desired RPM that's currently stored.
     *
     * @return Desired RPM
     */
    public double getRPMTarget() {
        return rpmTarget;
    }

    /**
     * This method will be called once per scheduler run.
     *
     * <p>Displays telemetry and sends power to the flywheel motor if needed.
     */
    @Override
    public void periodic() {
        if (isRunning) {
            setVelocity(rpmTarget);
        } else {
            turnOffFlywheel();
        }
        SmartDashboard.putNumber("Flywheel RPM", getVelocity());
        SmartDashboard.putBoolean("Is Shooter Running", isRunning);
        SmartDashboard.putNumber("rpmTarget", rpmTarget);
        SmartDashboard.putNumber("RPM error", Math.abs(rpmTarget - getVelocity()));
        if (Constants.DISPLAY_DIAGNOSTICS) {
            SmartDashboard.putNumber("Flywheel Temperature", flywheel.getTemperature());
            SmartDashboard.putNumber("Flywheel Volts", flywheel.getMotorOutputVoltage());
            SmartDashboard.putNumber(
                    "PID Error", flywheel.getClosedLoopError(Constants.Flywheel.PRIMARY_PID_LOOP));
            SmartDashboard.putNumber("FlywheelOutput", flywheel.get());
        }
    }

    /** Disables the {@link FlywheelSubsystem} by turning off the flywheel motor. */
    @Override
    public void disable() {
        setIsRunning(false);
        turnOffFlywheel();
    }
}
