package frc.robot.subsystems;

import frc.robot.RobotContainer;

/**
 * Allows subsystems to disable themselves (i.e: turn off motors) when the {@link RobotContainer}
 * calls {@code disableAllSubsystems()}
 */
public interface Disableable {
    public void disable();
}
