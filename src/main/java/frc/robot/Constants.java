// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import frc.robot.subsystems.*;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final int TEAM = 3786;

    public static final boolean DISPLAY_DIAGNOSTICS = false;
    public static final boolean ENABLE_TEST_BUTTON_MAPPINGS = false;

    /**
     * Contains constants specific for the {@link IntakeSubsystem}
     *
     * @author Tony P
     */
    public final class Intake {
        public static final int UP_DOWN_MOTOR_ID = 21;
        public static final int INTAKE_MOTOR_ID = 22;
        public static final int HALL_EFFECT_ID = 9;

        // Predetermined up position?
        public static final int UP_POSITION = 0;
        public static final int DOWN_POSITION = -281928;
        public static final double CALIBRATE_SPEED = 0.4;
        public static final double UP_DOWN_SPEED = 0.3;

        public static final double kF = 0.0;
        public static final double kP = 0.05;
        public static final double kI = 0.0;
        public static final double kD = 0.0;
        public static final double kTimeout = 0;
        public static final int PRIMARY_PID_LOOP =
                0; // Talon FX's have multiple slots for PID loops
        public static final String KP_KEY = "intake_p";
        public static final String KI_KEY = "intake_i";
        public static final String KD_KEY = "intake_d";
        public static final String KF_KEY = "intake_f";
        public static final String SETPOINT_KEY = "intake_setpoint";
        public static final double POSITION = 0.0;
    }

    /** Contains constants specific for the {@link ShooterHoodSubsystem} */
    public final class ShooterHood {
        public static final int SHOOTER_HOOD_MOTOR_ID = 32;
        public static final int PRIMARY_PID_LOOP = 0;
        public static final int MOTOR_CONFIG_TIMEOUT_MS = 30;
        public static final double MANUAL_SPEED = 0.1;
        public static final int HALL_EFFECT_SENSOR_PORT = 1;
        public static final double POSITION = 2000.0;
        public static final double CALIBRATE_SPEED = -0.1;
        public static final double ALLOWABLE_ERROR = 50.0;
        public static final double KP = 0.025;
        public static final double KI = 0.00007;
        public static final double KD = 0.4;
        public static final double KF = 0.0;
        public static final String KP_KEY = "hood_p";
        public static final String KI_KEY = "hood_i";
        public static final String KD_KEY = "hood_d";
        public static final String KF_KEY = "hood_f";
        public static final String SETPOINT_KEY = "hood_setpoint";
        public static final double INDEX_POSITION = 1589;
        public static final double LOW_GOAL_POSITION = 20612;
        public static final double TARMAC_LINE_POSITION = 7354;
    }

    /** Contains constants specific for the {@link KickerSubsystem} */
    public final class Kicker {
        public static final int KICKER_MOTOR_ID = 33;
        public static final double MANUAL_SPEED = 1;
        public static final double BACKTRACK_SPEED = -1;
    }

    /** Contains constants specific for Xbox Controllers */
    public final class XboxController {
        public static final int PRIMARY_ID = 0;
        public static final int SECONDARY_ID = 1;
        public static final double TRIGGER_BUTTON_THRESHOLD = 0.5;
    }

    /** Contains constants specific for the {@link DriveSubsystem} */
    public final class Drivetrain {
        public static final int LEFTREAR_ID = 12;
        public static final int LEFTFRONT_ID = 11;
        public static final int RIGHTREAR_ID = 14;
        public static final int RIGHTFRONT_ID = 13;
        public static final double OPEN_LOOP_RAMP = 0.4;
        public static final double NORMAL_SPEED = 0.35;
        public static final double BOOST_SPEED = 0.8; // ROBOT GO BRR
        public static final double SLOW_SPEED = 0.17;

        public static final boolean CURRENT_LIMIT_ENABLED = true;
        public static final double CURRENT_LIMIT = 30.0; // Amps
        public static final double CURRENT_LIMIT_TRIGGER_THRESHOLD_TIME = 1.0;
    }

    /** Contains Constants for the {@link FeederSubsystem} */
    public final class Feeder {
        public static final int FEEDER_MOTOR_ID = 22;
        public static final double FEED_SPEED = 0.5;
        public static final double BACKTRACK_SPEED = -1;
        public static final double BACKTRACK_TIMED_DURATION_MS = 50;
        public static final double TRIGGER_THRESHOLD = 0.5;
    }

    /**
     * Contains constants specific for the {@link FlywheelSubsystem}
     *
     * @author Nicholas L
     */
    public final class Flywheel {
        public static final int FLYWHEEL_ID = 31;
        public static final int MOTOR_CONFIG_TIMEOUT_MS = 30;
        public static final int PRIMARY_PID_LOOP =
                0; // Talon FX's have multiple slots for PID loops
        public static final double RAMP_RATE_SECONDS =
                0.3; // The time it takes to go from 0 to full power.
        public static final boolean CURRENT_LIMIT_ENABLED = true;
        public static final double CURRENT_LIMIT = 30.0; // In Amps, also used for the threshold
        public static final double CURRENT_LIMIT_TRIGGER_THRESHOLD_TIME = 1.0;
        public static final double KP = 0.1;
        public static final double KI = 0.0003;
        public static final double KD = 0.01;
        public static final double KF = 0.0;
        public static final double GEAR_REDUCTION = 0.5;
        public static final int RPM_CHANGE_STEP = 500;
        public static final double RPM_CONVERSION =
                3.4133; // Converts from desired RPM to encoder counts per 100 ms
        public static final String STATUS_KEY = "Flywheel Status";
        public static final String KP_KEY = "shooter_p";
        public static final String KI_KEY = "shooter_i";
        public static final String KD_KEY = "shooter_d";
        public static final String KF_KEY = "shooter_f";
        public static final String SETPOINT_KEY = "shooter_rpm_setpoint";

        public static final double INDEX_RPM = 3000;
        public static final double LOW_RPM = 1000;
        public static final double TARMAC_RPM = 3400;
        public static final double READY_TO_FIRE_THRESHOLD = 25.0; // was 25
    }

    /** Contains constants for the {@link ClimberSubsystem} */
    public final class Climber {
        public static final int MOTOR_ID = 41;
        public static final int HALL_EFFECT_ID = 0;
        public static final int PRIMARY_PID_LOOP = 0;
        public static final int MOTOR_CONFIG_TIMEOUT_MS = 30;

        public static final double KP = 0.05;
        public static final double KI = 0.0;
        public static final double KD = 0.0;
        public static final double KF = 0.0;

        public static final String KP_KEY = "climber_p";
        public static final String KI_KEY = "climber_i";
        public static final String KD_KEY = "climber_d";
        public static final String KF_KEY = "climber_f";

        public static final double CONTROL_SPEED = 0.25;
        public static final double CALIBRATE_SPEED = 0.25;

        public static final double LOW_BAR_POSITION = 85337.0;
        public static final double MID_BAR_POSITION = 235860.0;
        public static final double CLIMBED_POSITION = 18000.0;
    }

    /**
     * Contains constants specific for the {@link LimelightSubsystem}
     *
     * @author Nicholas L
     */
    public final class Limelight {
        public static final String NETWORKTABLE_NAME = "limelight";
        public static final String TV_INDEX = "tv";
        public static final String TX_INDEX = "tx";
        public static final String TY_INDEX = "ty";
        public static final String LEDMODE_INDEX = "ledMode";
        public static final String CAMMODE_INDEX = "camMode";
        public static final double FORCE_LEDS_OFF = 1.0;
        public static final double USE_PIPELINE_LED_MODE = 0.0;
        public static final double VISION_PROCESSING_MODE = 0.0;
        public static final double DRIVER_VISION_MODE = 1.0;
        public static final double ALIGNMENT_P = 0.015;
        public static final double ALIGNMENT_I = 0.0009;
        public static final double ALIGNMENT_D = 0.001;
        public static final double UPPER_HUB_HEIGHT = 2.64; // meters
        public static final double LIMELIGHT_HEIGHT = 0.866775; // meters
        public static final double LIMELIGHT_ANGLE = 26.0; // meters
        public static final double TX_TOLERANCE = 4.0;
        public static final double TARGET_AQUIRE_DELAY = 0.25; // seconds
        public static final double ERROR_CHANGE_RATE_TOLERANCE = 0.8; // degrees per 0.02 seconds
    }

    // These are for Config.java
    public static final String DATA_STORAGE_PATH = "/home/lvuser";
    public static final String CONFIG_FILE_NAME = "config.yml";
}
