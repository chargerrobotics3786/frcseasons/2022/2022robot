// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.Climber.ClimberAutoCommand;
import frc.robot.commands.Climber.ClimberCalibrateCommand;
import frc.robot.commands.Climber.ClimberManualCommand;
import frc.robot.commands.Climber.ClimberZeroCommand;
import frc.robot.commands.CommandGroups.FaceTargetCommandGroup;
import frc.robot.commands.CommandGroups.IndexShotCommandGroup;
import frc.robot.commands.CommandGroups.LowShotCommandGroup;
import frc.robot.commands.CommandGroups.ShooterOffCommandGroup;
import frc.robot.commands.CommandGroups.TarmacShotCommandGroup;
import frc.robot.commands.Drivetrain.AutoDriveTimedCommand;
import frc.robot.commands.Drivetrain.AutoTurnL180Command;
import frc.robot.commands.Drivetrain.BoostCommand;
import frc.robot.commands.Drivetrain.BrakeCommand;
import frc.robot.commands.Drivetrain.ManualDrive;
import frc.robot.commands.Drivetrain.SlowCommand;
import frc.robot.commands.Feeder.FeederOffCommand;
import frc.robot.commands.Feeder.FeederOnCommand;
import frc.robot.commands.Feeder.FireWhenReadyCommand;
import frc.robot.commands.Flywheel.FlywheelOffCommand;
import frc.robot.commands.Flywheel.FlywheelOnCommand;
import frc.robot.commands.Flywheel.FlywheelSetpointCommand;
import frc.robot.commands.Intake.IntakeAutoCommand;
import frc.robot.commands.Intake.IntakeManualCommand;
import frc.robot.commands.Intake.IntakeZeroCommand;
import frc.robot.commands.Limelight.LimelightOffCommand;
import frc.robot.commands.Limelight.TurnToTargetCommand;
import frc.robot.commands.autonomous.ShootOneBallMoveBackwards;
import frc.robot.commands.autonomous.TwoBallCommand;
import frc.robot.commands.kicker.KickerOffCommand;
import frc.robot.commands.kicker.KickerOnCommand;
import frc.robot.commands.shooterhood.HoodCalibrateCommand;
import frc.robot.commands.shooterhood.HoodManualCommand;
import frc.robot.subsystems.ClimberSubsystem;
import frc.robot.subsystems.Disableable;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.FeederSubsystem;
import frc.robot.subsystems.FlywheelSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import frc.robot.subsystems.LimelightSubsystem;
import frc.robot.subsystems.ShooterHoodSubsystem;
import frc.robot.utils.Config;
import frc.robot.utils.XboxController;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    // The robot's subsystems and commands are defined here...
    private ShooterHoodSubsystem m_ShooterHoodSubsystem;
    private DriveSubsystem m_DriveSubsystem;
    private HoodManualCommand hoodManualUpCommand;
    private HoodManualCommand hoodManualDownCommand;

    private FlywheelSubsystem flywheelSubsystem;
    private FlywheelOnCommand flywheelOnCommand;
    private FlywheelOffCommand flywheelOffCommand;
    private FlywheelSetpointCommand flywheelIncrementCommand;
    private FlywheelSetpointCommand flywheelDecrementCommand;

    private HoodCalibrateCommand hoodCalibrateCommand;
    private ManualDrive manualDrive;
    private BrakeCommand brakeCommand;
    private BoostCommand boostCommand;
    private SlowCommand slowCommand;

    // Intake
    private IntakeSubsystem intakeSubsystem;
    private IntakeAutoCommand intakeUpAutoCommand;
    private IntakeAutoCommand intakeDownAutoCommand;

    private IntakeZeroCommand intakeZeroCommand;
    private IntakeManualCommand intakeUpManualCommand;
    private IntakeManualCommand intakeDownManualCommand;
    // Kicker
    private KickerSubsystem kickerSubsystem;
    private KickerOnCommand kickerOnCommand;
    private KickerOffCommand kickerOffCommand;
    // Limelight
    private LimelightSubsystem limelightSubsystem;
    private FaceTargetCommandGroup faceTargetCommandGroup;
    private TurnToTargetCommand turnToTargetCommand;
    private LimelightOffCommand limelightOffCommand;

    // Climber
    private ClimberSubsystem climberSubsystem;
    private ClimberCalibrateCommand climberCalibrateCommand;
    private ClimberZeroCommand climberZeroCommand;
    private ClimberManualCommand climberDownCommand;
    private ClimberManualCommand climberUpCommand;
    private ClimberAutoCommand climberToLowBarCommand;
    private ClimberAutoCommand climberToMidBarCommand;
    private ClimberAutoCommand climberToDownPositionCommand;

    // Feeder
    private FeederSubsystem feederSubsystem;
    private FeederOnCommand feederOnCommand;
    private FeederOffCommand feederOffCommand;
    private FeederOnCommand feederBacktrackCommand;
    private FireWhenReadyCommand fireWhenReadyCommand;

    // Autonomous
    private Command oneBallAuto;
    private Command wait15sec;
    private Command taxiAutoCommand;
    private Command turn180Command;
    private Command twoBallAuto;

    private ShooterOffCommandGroup shooterOffCommandGroup;
    private TarmacShotCommandGroup tarmacShotCommandGroup;
    private IndexShotCommandGroup indexShotCommandGroup;
    private LowShotCommandGroup lowShotCommandGroup;

    // Chooser for autonomous commands
    SendableChooser<Command> chooser = new SendableChooser<>();

    // Xbox Controllers
    public XboxController primary;
    public XboxController secondary;

    /** The container for the robot. Contains subsystems, OI devices, and commands. */
    public RobotContainer() {
        primary = XboxController.getInstance(Constants.XboxController.PRIMARY_ID);
        secondary = XboxController.getInstance(Constants.XboxController.SECONDARY_ID);
        setUpSubsystemsAndCommands();
        configureButtonBindings();
        setupCommandChooser();
        SmartDashboard.putData(chooser);
    }

    // The robot's subsystems and commands are defined here...
    private void setUpSubsystemsAndCommands() {
        Config.setup();
        flywheelSubsystem = FlywheelSubsystem.getInstance();
        intakeSubsystem = IntakeSubsystem.getInstance();
        flywheelOnCommand =
                new FlywheelOnCommand(flywheelSubsystem, flywheelSubsystem.getRPMTarget());
        flywheelOffCommand = new FlywheelOffCommand(flywheelSubsystem);
        flywheelIncrementCommand =
                new FlywheelSetpointCommand(flywheelSubsystem, Constants.Flywheel.RPM_CHANGE_STEP);
        flywheelDecrementCommand =
                new FlywheelSetpointCommand(flywheelSubsystem, -Constants.Flywheel.RPM_CHANGE_STEP);

        intakeZeroCommand = new IntakeZeroCommand(intakeSubsystem);
        intakeUpManualCommand = new IntakeManualCommand(intakeSubsystem, true);
        intakeDownManualCommand = new IntakeManualCommand(intakeSubsystem, false);
        intakeUpAutoCommand = new IntakeAutoCommand(intakeSubsystem, true);
        intakeDownAutoCommand = new IntakeAutoCommand(intakeSubsystem, false);

        climberSubsystem = ClimberSubsystem.getInstance();
        climberDownCommand = new ClimberManualCommand(climberSubsystem, false);
        climberUpCommand = new ClimberManualCommand(climberSubsystem, true);
        climberCalibrateCommand = new ClimberCalibrateCommand(climberSubsystem);
        climberZeroCommand = new ClimberZeroCommand(climberSubsystem);
        climberToLowBarCommand =
                new ClimberAutoCommand(climberSubsystem, Constants.Climber.LOW_BAR_POSITION);
        climberToMidBarCommand =
                new ClimberAutoCommand(climberSubsystem, Constants.Climber.MID_BAR_POSITION);
        climberToDownPositionCommand =
                new ClimberAutoCommand(climberSubsystem, Constants.Climber.CLIMBED_POSITION);

        feederSubsystem = FeederSubsystem.getInstance();
        feederOnCommand = new FeederOnCommand(feederSubsystem, true);
        feederOffCommand = new FeederOffCommand(feederSubsystem);
        feederBacktrackCommand = new FeederOnCommand(feederSubsystem, false);

        fireWhenReadyCommand = new FireWhenReadyCommand(flywheelSubsystem, feederSubsystem);

        kickerSubsystem = KickerSubsystem.getInstance();
        kickerOnCommand = new KickerOnCommand(kickerSubsystem);
        kickerOffCommand = new KickerOffCommand(kickerSubsystem);

        m_ShooterHoodSubsystem = ShooterHoodSubsystem.getInstance();
        hoodManualUpCommand = new HoodManualCommand(m_ShooterHoodSubsystem, true);
        hoodManualDownCommand = new HoodManualCommand(m_ShooterHoodSubsystem, false);
        hoodCalibrateCommand = new HoodCalibrateCommand(m_ShooterHoodSubsystem);

        m_DriveSubsystem = DriveSubsystem.getInstance(primary);
        manualDrive = new ManualDrive(m_DriveSubsystem, primary);
        brakeCommand = new BrakeCommand(m_DriveSubsystem, primary);
        boostCommand = new BoostCommand(m_DriveSubsystem, primary);
        slowCommand = new SlowCommand(m_DriveSubsystem, primary);
        CommandScheduler.getInstance().setDefaultCommand(m_DriveSubsystem, manualDrive);

        limelightSubsystem = LimelightSubsystem.getInstance();
        faceTargetCommandGroup = new FaceTargetCommandGroup(limelightSubsystem, m_DriveSubsystem);
        turnToTargetCommand = new TurnToTargetCommand(limelightSubsystem, m_DriveSubsystem);
        limelightOffCommand = new LimelightOffCommand(limelightSubsystem);

        oneBallAuto =
                new ShootOneBallMoveBackwards(
                        m_DriveSubsystem,
                        feederSubsystem,
                        kickerSubsystem,
                        flywheelSubsystem,
                        m_ShooterHoodSubsystem,
                        intakeSubsystem);
        wait15sec = new WaitCommand(15);
        taxiAutoCommand = new AutoDriveTimedCommand(m_DriveSubsystem);
        turn180Command =
                new AutoTurnL180Command(m_DriveSubsystem)
                        .andThen(new FaceTargetCommandGroup(limelightSubsystem, m_DriveSubsystem));
        twoBallAuto =
                new TwoBallCommand(
                        m_DriveSubsystem,
                        feederSubsystem,
                        kickerSubsystem,
                        flywheelSubsystem,
                        m_ShooterHoodSubsystem,
                        intakeSubsystem,
                        limelightSubsystem);

        shooterOffCommandGroup =
                new ShooterOffCommandGroup(
                        flywheelSubsystem,
                        m_ShooterHoodSubsystem,
                        kickerSubsystem,
                        feederSubsystem,
                        limelightSubsystem);
        tarmacShotCommandGroup =
                new TarmacShotCommandGroup(
                        feederSubsystem,
                        kickerSubsystem,
                        m_ShooterHoodSubsystem,
                        flywheelSubsystem,
                        limelightSubsystem,
                        m_DriveSubsystem);
        indexShotCommandGroup =
                new IndexShotCommandGroup(
                        feederSubsystem,
                        kickerSubsystem,
                        m_ShooterHoodSubsystem,
                        flywheelSubsystem);
        lowShotCommandGroup =
                new LowShotCommandGroup(
                        feederSubsystem,
                        kickerSubsystem,
                        m_ShooterHoodSubsystem,
                        flywheelSubsystem);
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be created by RICK
     * ASTLEY(!?) instantiating a {@link GenericHID} or one of its subclasses ({@link
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
     * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {
        // primary, I want these mappings to stay the same between tests and matches - Nicholas
        primary.buttonB.whileHeld(brakeCommand);
        primary.buttonX.whenPressed(faceTargetCommandGroup);
        primary.buttonBumperRight.whileHeld(
                boostCommand); // can also use whenHeld but we haven't tested the difference yet
        primary.buttonBumperLeft.whileHeld(slowCommand);

        if (Constants.ENABLE_TEST_BUTTON_MAPPINGS) {
            SmartDashboard.putString("ButtonMappingScheme", "TEST");
            // primary controller mappings for testing
            primary.buttonPovLeft.whenPressed(intakeDownAutoCommand);
            primary.buttonPovRight.whenPressed(intakeUpAutoCommand);

            // secondary controller mappings for testing
            secondary.buttonA.whenPressed(flywheelOnCommand);
            secondary.buttonB.whenPressed(flywheelOffCommand);
            secondary.buttonPovUp.whenPressed(flywheelIncrementCommand);
            secondary.buttonPovDown.whenPressed(flywheelDecrementCommand);
            secondary.buttonPovRight.whileHeld(hoodManualUpCommand);
            secondary.buttonPovLeft.whileHeld(hoodManualDownCommand);
            secondary.buttonMenu.whenPressed(hoodCalibrateCommand);
            secondary.buttonStickLeft.whileHeld(feederOnCommand);
            secondary.buttonStickLeft.whenReleased(feederOffCommand);
            secondary.buttonStickRight.whileHeld(feederBacktrackCommand);
            secondary.buttonStickRight.whenReleased(feederOffCommand);
        } else {
            SmartDashboard.putString("ButtonMappingScheme", "COMPETITION");
            // primary controller mappings for matches
            primary.buttonPovLeft.whileHeld(climberDownCommand);
            primary.buttonView.whenPressed(climberZeroCommand);
            primary.buttonPovUp.whenPressed(climberToMidBarCommand);
            primary.buttonPovRight.whenPressed(climberToLowBarCommand);
            primary.buttonPovDown.whenPressed(climberToDownPositionCommand);
            primary.buttonY.whileHeld(turnToTargetCommand);
            primary.buttonY.whenReleased(limelightOffCommand);

            // secondary controller mappings for matches
            secondary.buttonMenu.whenPressed(hoodCalibrateCommand);
            secondary.buttonView.whenPressed(intakeZeroCommand);
            secondary.buttonA.whenPressed(lowShotCommandGroup);
            secondary.buttonX.whenPressed(indexShotCommandGroup);
            secondary.buttonY.whenPressed(tarmacShotCommandGroup);
            secondary.buttonB.whenPressed(shooterOffCommandGroup);
            secondary.buttonPovDown.whileHeld(intakeDownAutoCommand);
            secondary.buttonPovUp.whileHeld(intakeUpAutoCommand);
            secondary.buttonPovLeft.whileHeld(climberDownCommand);
            secondary.buttonPovRight.whileHeld(climberUpCommand);
            secondary.buttonBumperRight.whileHeld(feederOnCommand);
            secondary.buttonBumperRight.whenReleased(feederOffCommand);
            secondary.buttonBumperLeft.whileHeld(feederBacktrackCommand);
            secondary.buttonBumperLeft.whenReleased(feederOffCommand);
            secondary.buttonStickLeft.whileHeld(intakeUpManualCommand);
            secondary.rightTriggerButton.whenHeld(fireWhenReadyCommand, false);
        }
    }

    public void setupCommandChooser() {
        chooser.setDefaultOption("2 Ball Auto", twoBallAuto);
        chooser.addOption("1 ball auto", oneBallAuto);
        chooser.addOption("Wait 15 seconds", wait15sec);
        chooser.addOption("Taxi off line", taxiAutoCommand);
        chooser.addOption("Turn -180", turn180Command);
    }

    /**
     * Sets the selected command from the chooser to be the one that will run during the autonomous
     * phase.
     *
     * @return The selected command
     */
    public Command getAutonomousCommand() {
        return chooser.getSelected();
    }

    /** Schedules the manual drive command and turns off the limelight. */
    public void setTeleop() {
        manualDrive.schedule();
        limelightSubsystem.setLEDStatus(false);
    }

    /** Calls {@code disable()} in subsystems that implement {@link Disableable} */
    public void disableAllSubsystems() {
        flywheelSubsystem.disable();
        kickerSubsystem.disable();
        m_DriveSubsystem.disable();
        limelightSubsystem.disable();
        m_ShooterHoodSubsystem.disable();
        climberSubsystem.disable();
    }
}
